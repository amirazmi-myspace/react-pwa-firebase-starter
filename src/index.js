import ReactDOM from 'react-dom'
import * as serviceWorkerRegistration from './serviceWorkerRegistration'
import './assets/main.css'
import { ProvideAuth } from './contexts/AuthContext'

const App = () => {
  return (
    <div className='flex justify-center items-center h-screen'>
      <h1 className='text-2xl'>Template for React, TailwindCSS and Firebase</h1>
    </div>
  )
}

ReactDOM.render(<ProvideAuth><App /></ProvideAuth>, document.getElementById('root'))
serviceWorkerRegistration.unregister()