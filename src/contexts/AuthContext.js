import { createContext, useContext, useEffect, useState } from "react"
import FB, { AUTH } from "../configs/firebase"

const AuthContext = createContext()
export const useAuth = () => useContext(AuthContext)

export const ProvideAuth = ({ children }) => {
  const [user, setUser] = useState(false)
  const [load, setLoad] = useState(true)

  const signup = (emailInput, passwordInput) => AUTH.createUserWithEmailAndPassword(emailInput, passwordInput)
  const signin = (email, password) => AUTH.signInWithEmailAndPassword(email, password)
  const logout = () => AUTH.signOut()
  const resetPassword = (email) => AUTH.sendPasswordResetEmail(email)
  const updateEmail = (email) => user.updateEmail(email)
  const updatePassword = (password) => user.updatePassword(password)

  useEffect(() => {
    AUTH.setPersistence(FB.auth.Auth.Persistence.SESSION)
    const unsubsribe = AUTH.onAuthStateChanged(user => {
      setUser(user)
      setLoad(false)
    })
    return unsubsribe
  }, [])

  const value = { user, signup, signin, logout, resetPassword, updateEmail, updatePassword }

  return <AuthContext.Provider value={value}>{!load && children}</AuthContext.Provider>
}
